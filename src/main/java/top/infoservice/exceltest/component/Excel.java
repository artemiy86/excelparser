package top.infoservice.exceltest.component;

import lombok.SneakyThrows;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

@ShellComponent
public class Excel {
    private org.apache.poi.ss.usermodel.CellType CellType;

    @SneakyThrows
    @ShellMethod
    public void excelread(){
        FileInputStream file = new FileInputStream(ResourceUtils.getFile("classpath:files/ecola.xlsx"));
        //OPCPackage file = OPCPackage.open(ResourceUtils.getFile("classpath:files/ecola.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        Sheet sheet = workbook.getSheetAt(0);
        int cnt = 0;
        for (Row row : sheet) {
            int cellCnt = 0;
            for (Cell cell : row) {

                CellType cellType = cell.getCellType();
                if (cellType == STRING){
                    String value = cell.getRichStringCellValue().getString();
                    System.out.print(value + " ");
                }else if (cellType == NUMERIC) {
                    double value = cell.getNumericCellValue();
                    System.out.print(value + " ");
                }

            }
            System.out.println("");
            //if (cnt > 5) return;
            cnt++;
        }
        System.out.println(cnt);
    }
}
